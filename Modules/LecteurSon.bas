Attribute VB_Name = "LecteurSon"
Option Explicit

'On d�clare la fonction sndPlaySoundA pr�sente dans la librairy winmm.dll. On change son nom en play
'Elle prends en argument le nom du fichier (obligatoirement un .wav) et des flags
Private Declare Function play Lib "winmm.dll" Alias "sndPlaySoundA" (ByVal lpszSoundName As String, ByVal uFlags As Long) As Long

'Valeurs des flags en hexad�cimal
Private Const SND_SYNC = &H0
Private Const SND_ASYNC = &H1
Private Const SND_NODEFAULT = &H2

Sub JouerSon1(son As String)
    
    'on joue le son pr�sent dans le dossier SONS en asyncrone
    play ThisWorkbook.path & "\SONS\" & son & ".wav", SND_ASYNC
End Sub

Sub StopSon()
    play SND_SYNC, SND_SYNC + SND_NODEFAULT
End Sub
