Attribute VB_Name = "Partition"
Option Explicit

Public Function PlacerNote(selec As Range)

    Dim j As Integer
    
    If EstDansLaPartition(selec) Then 'On v�rifie que la note soit dans la plage du graph
        If (selec.Rows.count = 1) Then 'Si la selection fait 1 lignes de hauteur on place les notes dans la selection
        
            'On supprime les notes sur la colonnes avant de placer les nouvelles note
            For j = selec.Column To selec.Columns.count + selec.Column - 1
                
                Dim r As Range
                Set r = Utilitaire.construireRange(Utilitaire.LigneMinimumPartition, j, Utilitaire.LigneMaximumPartition, j)
                
                Utilitaire.ResetCouleur r, True
            Next j
                
            'On place les nouvelles notes
            selec.Interior.Color = vbGreen
            
            LecteurSon.JouerSon1 (Utilitaire.convertirNombreLigneEnNote(selec.Row))
            
            Exit Function 'On sort de la fonction
        End If
          
        'Si la selection fait plus de 1 ligne de hauteur alors on suprrime les notes dans la selection
        Utilitaire.ResetCouleur selec, True
    End If

End Function

Sub nettoyer()
    
    'D�claration des variables
    Dim partition As Range
    Dim i As Integer

    'On construit une selection qui correspond a tout le graph
    Set partition = Range( _
        Cells(Utilitaire.LigneMinimumPartition, Utilitaire.ColonneMinimumPartition), _
        Cells(Utilitaire.LigneMaximumPartition, Utilitaire.ColonneMaximumPartition))
        
    'On enleve les couleurs
    partition.Interior.Color = xlNone
    
    'On remet les lignes grises en fond
    For i = Utilitaire.LigneMinimumPartition To Utilitaire.LigneMaximumPartition
        If Cells(i, Utilitaire.ColonneMinimumPartition - 1).Interior.Color = vbBlack Then 'Si la note sur le piano dessin� � gauche est noir, on colorie la ligne en gris
            construireRange(i, Utilitaire.ColonneMinimumPartition, i, Utilitaire.ColonneMaximumPartition).Interior.Color = RGB(220, 220, 220)
        End If

    Next i

End Sub

Private Function EstDansLaPartition(selec As Range) As Boolean
    
    EstDansLaPartition = selec.Row <= Utilitaire.LigneMaximumPartition _
                     And selec.Row >= Utilitaire.LigneMinimumPartition _
                     And selec.Column <= Utilitaire.ColonneMaximumPartition _
                     And selec.Column >= Utilitaire.ColonneMinimumPartition

End Function
