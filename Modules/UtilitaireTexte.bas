Attribute VB_Name = "UtilitaireTexte"
Option Explicit

Function lireTexte(fichier As String) As String

    'D�claration des variables
    Dim texte, ligne As String
    Dim IndexFichier As Integer

    'On r�cup�re une r�f�rence (IndexFichier) sur le fichier
    IndexFichier = FreeFile()
    Open ActiveWorkbook.path & "\" & fichier For Input As #IndexFichier 'ouvre le fichier

    'On r�cup�re la totalit� du texte contenu dans le fichier
    texte = ""
    Do While Not EOF(IndexFichier) 'EOF retourne False tant que la fin du fichier n'est pas atteinte
        Line Input #IndexFichier, ligne
        
        If texte <> "" Then
            texte = texte & "||" & ligne  'On utilise le s�parateur || pour distinguer les lignes
        Else
            texte = ligne
        End If
    Loop

    'On ferme le fichier
    Close #IndexFichier

    lireTexte = texte
    
End Function

Function EcrireTexte(NomFichier As String, texte As String)
    
    Dim IndexFichier As Integer
    
    'On r�cup�re une r�f�rence (IndexFichier) sur le fichier
    IndexFichier = FreeFile()
    Open ThisWorkbook.path & "\" & NomFichier For Output As #IndexFichier
    
    'On �crit dans le fichier le texte
    Print #IndexFichier, texte
    
    'On ferme le fichier
    Close #IndexFichier

End Function
