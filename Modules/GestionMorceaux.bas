Attribute VB_Name = "GestionMorceaux"
Option Explicit

'Conversion morceau dans fichier texte en morceau sur excel
Sub chargerMorceau(nomMorceau As String)
    partition.nettoyer

    'D�claration des variables
    Dim texte As String
    Dim tableauMorceau() As String
    Dim k, j As Integer
    
    'On r�cupere le texte dans le fichier
    texte = UtilitaireTexte.lireTexte("MORCEAUX\" & nomMorceau)
    tableauMorceau = Split(texte, "||") 'On le split le texte pour avoir un tableau contenant le texte ligne par ligne


    'On parcourt toute les lignes du tabelau de notes, donc temps par temps
    For j = LBound(tableauMorceau) To UBound(tableauMorceau)
        
        Dim temp As Integer
        Dim notes() As String
        
        'Pr�venir erreur venant d'une ligne vide ou d'un ligne contant des inforamtions ex : nom du morceau
        'Format correct : A:B si on split dans un tableau par rapport � ":" la taille du tableau est de 1
        If (UBound(Split(tableauMorceau(j), ":")) = 1) Then
        
            
            temp = Split(tableauMorceau(j), ":")(0) 'on r�cup�re le temps sur l'index 0 du tableau
            notes = Split(Split(tableauMorceau(j), ":")(1), ",") 'on r�cup�re les notes sur l'index 1 qu'on split ensuite par rapport "," pour l'avoir en liste
            
            
            'On parcours toutes les notes sur chaque temps
            For k = LBound(notes) To UBound(notes)
                
                'On d�duit la ligne de la case a colorier
                Dim ligne As Integer
                ligne = Utilitaire.convertirNoteEnNombreDeLigne(notes(k))
                
                'On d�duit la colonne de la case a colorier
                Dim colonne As Integer
                colonne = Utilitaire.ColonneMinimumPartition + temp - 1
                
                'On colore les notes sur le graph
                Cells(ligne, colonne).Interior.Color = vbGreen
                
                
            Next k
        
        End If
    Next j
    
    
End Sub

Sub enregister()

    'D�claration des variables
    Dim i, j As Integer
    Dim texte As String

    'On montre la fenetre fermetant de sauvergarder le morceau
    SaveForm.Show
    
    'Format de sauvegarde du morceau :
    'TITRRE
    'ARTISTE
    'BPM
    'TEMPS:NOTE,NOTE
    
    If (SaveForm.Titre = "" Or SaveForm.Artiste = "" Or SaveForm.Tempo = "") Then
        Exit Sub
    End If
    
    
    'On construit le texte
    texte = SaveForm.Titre.Value & vbNewLine & SaveForm.Artiste.Value & vbNewLine & SaveForm.Tempo.Value & vbNewLine

    'Construction du morceau sous forme de texte
    For j = Utilitaire.ColonneMinimumPartition To Utilitaire.ColonneMaximumPartition
        For i = Utilitaire.LigneMinimumPartition To Utilitaire.LigneMaximumPartition
        
            
            If (Cells(i, j).Interior.Color = vbGreen) Then
                
                'Conversion du num�ro de la ligne, correspondant � la note, en nom de note
                Dim nomNote As String
                nomNote = Utilitaire.convertirNombreLigneEnNote(i)
            
                'Conversion du num�ro de la colonne en numero de temps (d�calage au niveau de la colonne de d�but)
                Dim temps As Integer
                temps = j - Utilitaire.ColonneMinimumPartition + 1
                
                'on ajoute la note dans le texte
                texte = texte & temps & ":" & nomNote & vbNewLine
            End If
            
        
        Next i
    Next j

    'On enregistre le texte dans un fichier
    UtilitaireTexte.EcrireTexte "\MORCEAUX\" & SaveForm.Titre.Value & ".txt", texte
    
End Sub

'Permet de r�cupere les informations sous forme d'un tableau d'objet qui ont pour attribues les informations
Public Function recupererInformationsMorceaux() As Morceau()

    'D�claration des variables
    Dim listeMorceaux() As Morceau
    Dim Lecteur As LecteurDossier
    Dim nomFichierMorceaux() As String
    
    Set Lecteur = New LecteurDossier 'On initialise l'objet
    Lecteur.path = ThisWorkbook.path & "/MORCEAUX/" 'On d�finit le chemin
    nomFichierMorceaux = Lecteur.listerFichiers 'On r�cupere tout les noms de fichier dans un tableau
    
    ReDim listeMorceaux(UBound(nomFichierMorceaux)) 'On d�finit la taille du tableau d'objet �gal au nombre de morceau
    
    
    Dim i As Integer
    
    'On peuple le tableau
    For i = LBound(nomFichierMorceaux) To UBound(nomFichierMorceaux) - 1
    
        'D�claration des variables
        Dim Titre As String
        Dim Tempo As Integer
        Dim Artiste As String
        Dim contenu() As String
        
        'On r�cupere le contenue du fichier texte qu'on split par rapport � "||" pour le mettre dans un tableau contenant les lignes du fichier
        contenu = Split(UtilitaireTexte.lireTexte("\MORCEAUX\" & nomFichierMorceaux(i)), "||")
        
        'On r�cup�re les m�tadonn�es du morceau
        Titre = contenu(0)
        Artiste = contenu(1)
        Tempo = contenu(2)
        
        'On initialise l'objet avec les informations
        Dim Morceau As Morceau
        Set Morceau = New Morceau
        Morceau.init Titre, Tempo, Artiste, nomFichierMorceaux(i)
        
        Set listeMorceaux(i) = Morceau 'On ajoute le morceau au tableau
    Next i

    recupererInformationsMorceaux = listeMorceaux 'On retourne le tableau
End Function

