Attribute VB_Name = "LecteurMusique"
Option Explicit

'Variable permettant de d�finir l'�tat(en marche ou pas)
Dim run As Boolean

'Permet de jouer le morceau en parcourant le graph
Sub parcourir()
    
    'Permet d'�viter de relancer la musique quand elle est d�ja en cours de lecture
    If (run) Then
        Exit Sub
    End If
    
    
    'Le premier son jou� est toujours � moiti� coup� lors de la lecture, le son START qui correspond � un enregistrement sans son est donc lanc� avant
    LecteurSon.JouerSon1 "START"
    run = True

    Dim i, j As Integer

    For j = Utilitaire.ColonneMinimumPartition - 1 To Utilitaire.ColonneMaximumPartition 'On parcours toutes les colonnes, le -1 permet de commencer l'it�rations avant et de na pas avoir le premier son coup�
        MettreEnEvidence (j)
        For i = Utilitaire.LigneMinimumPartition To Utilitaire.LigneMaximumPartition 'On parcours toutes les lignes de la colonne
            
            'Si l'�tat est � false (on arrette la lecture) on sort de la deuxi�me boucle en stoppant le son et en enlevant la barre rouge
            If run = False Then
                EnleverEnEvidence (j)
                LecteurSon.StopSon
                Exit For
            End If
            
            
            ' Jouer les notes  si le fond des cellules est vert
            If Cells(i, j).Interior.Color = vbGreen Then
                LecteurSon.JouerSon1 (Utilitaire.convertirNombreLigneEnNote(i))
            End If
        
        Next i
        
        'On attend entre chaque note en fonction du tempo
        Utilitaire.WaitFor (Utilitaire.convertirTempoEnTemps(Feuil1.TextBoxBPM.Value) / 4) 'On convertie le tempo en temps d'une noir et on divise par 4 car le temps et lui m�me subdivis� par 4
        EnleverEnEvidence (j)
        
        'Permet de sortir de la premi�re boucle si l'�tat est a false
        If run = False Then
                Exit For
        End If
    Next j
    

End Sub

Private Sub MettreEnEvidence(colonne As Integer)

    'Verification que la colonne soit dans la plage du graph
    If colonne < Utilitaire.ColonneMinimumPartition Or colonne > Utilitaire.ColonneMaximumPartition Then
        Exit Sub
    End If

    'On colorie en rouge toute la colonne du graph sauf les cellules vertes
    Dim i As Integer
    For i = Utilitaire.LigneMinimumPartition To Utilitaire.LigneMaximumPartition
    
        If Not Cells(i, colonne).Interior.Color = vbGreen Then
            Cells(i, colonne).Interior.Color = vbRed
        End If
        
    Next i
    
End Sub

Private Sub EnleverEnEvidence(colonne As Integer)

    'Verification que la colonne soit dans la plage du graph
    If colonne < Utilitaire.ColonneMinimumPartition Or colonne > Utilitaire.ColonneMaximumPartition Then
        Exit Sub
    End If

    'On convertie la colonne en selection
    Dim r As Range
    Set r = Utilitaire.construireRange(Utilitaire.LigneMinimumPartition, colonne, Utilitaire.LigneMaximumPartition, colonne)
    Utilitaire.ResetCouleur r, False 'On reset les couleurs sans toucher aux cellules vertes (d'o� le false)

End Sub

Public Sub stopMusique()
    run = False 'on d�finit l'�tat � false
    LecteurSon.StopSon 'on stop le son en cours
End Sub

