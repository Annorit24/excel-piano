Attribute VB_Name = "AfficheurMorceau"
Option Explicit

Private Const nbrDeMorceauxParLigne As Integer = 6
Private Const ligneDebut As Integer = 4
Private Const colonneDebut As Integer = 3

'On utilise une map pour enregistrer l'association entre le titre du morceau et le nom du fichier
Public nomFichiersMorceaux As Dictionary

'Affichage de la liste des morceaux dans les cellules
Public Function afficherMorceaux()

    'D�claration des variables
    Dim nbrDeLigne As Integer
    Dim morceaux() As Morceau
    Dim k As Integer
    Dim ligne As Integer
    Dim colonne As Integer
    
    Set nomFichiersMorceaux = New Dictionary 'On initilise l'objet
    
    Cells.Clear
    morceaux = GestionMorceaux.recupererInformationsMorceaux 'On r�cupere les informations sous forme d'un tableau d'objet qui ont pour attribues les informations
    nbrDeLigne = Application.WorksheetFunction.RoundUp(UBound(morceaux) / nbrDeMorceauxParLigne, 0) 'On calcul sur combien de ligne on va affichier les morceaux sachant qu'il y a 6 morceaux par lignes

    formaterLignes (nbrDeLigne) 'On formate les lignes (hauteur et largeur)
    
    ligne = ligneDebut
    colonne = colonneDebut
    
    'On afficher les morceaux dans les cellules
    For k = LBound(morceaux) To UBound(morceaux) - 1
        
        nomFichiersMorceaux(morceaux(k).Titre) = morceaux(k).NomFichier 'On enregistre la correspondance "titre -> nom fichier" de notre morceau
        
        Cells(ligne, colonne).Interior.Color = RGB(91, 172, 50)
        Cells(ligne, colonne).Value = morceaux(k).Titre & "." & vbNewLine & morceaux(k).Artiste & "." & vbNewLine & morceaux(k).Tempo & " BPM" 'On met des points entre les donn�es pour pouvoir le split plus facilement apr�s
        Cells(ligne, colonne).HorizontalAlignment = xlCenter
        Cells(ligne, colonne).VerticalAlignment = xlCenter
        
        colonne = colonne + 2
        
        If colonne = colonneDebut + nbrDeMorceauxParLigne * 2 Then
            colonne = colonneDebut
            ligne = ligne + 2
        End If
        
    Next k
    
End Function


Public Function formaterLignes(nbrDeLignes As Integer)

    Dim i, j As Integer
    
    'Formatage des hauteurs de lignes
    For i = 1 To nbrDeLignes
        Rows(2 + i * 2).RowHeight = 100
        Rows(3 + i * 2).RowHeight = 64
    Next i
    
    'Formatage des largeur de colonnes
    For j = 1 To nbrDeMorceauxParLigne
        Columns(1 + j * 2).ColumnWidth = 18.43
        Columns(2 + j * 2).ColumnWidth = 11.43
    Next j


End Function

'Permet de r�cuperer l'�quivalence entre titre et nom du fichier
Public Function recupererNomFichierMorceau(Titre As String) As String

    recupererNomFichierMorceau = nomFichiersMorceaux.Item(Titre)

End Function
