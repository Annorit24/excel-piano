Attribute VB_Name = "Utilitaire"
Public Const ColonneMinimumPartition As Integer = 3
Public Const ColonneMaximumPartition As Integer = 62
Public Const LigneMinimumPartition As Integer = 7
Public Const LigneMaximumPartition As Integer = 31

Private notes(LigneMaximumPartition) As String

Public Function convertirNombreLigneEnNote(ByVal ligneNote As Integer) As String

    'Permet de charger la liste si ce n'est pas d�ja fait
    If notes(LigneMinimumPartition) = "" Then
        RemplireNotes
    End If
    
    'Si la ligne est dans la plage du graph, on retourne la note correspondante enregistr�e dans le tableau sinon on retourne "ERROR"
    If ligneNote <= LigneMaximumPartition And ligneNote >= LigneMinimumPartition Then
        convertirNombreLigneEnNote = notes(ligneNote)
    Else
        convertirNombreLigneEnNote = "ERROR"
    End If

End Function

Public Function convertirNoteEnNombreDeLigne(ByVal note As String) As Integer

    'Permet de charger la liste si ce n'est pas d�ja fait
    If notes(LigneMinimumPartition) = "" Then
        RemplireNotes
    End If

    'On parourt notre tableau de note pour savoir si la note est pr�sente. Si on la trouve, on retourne l'index qui correspond au num�ro de ligne
    For i = LBound(notes) To UBound(notes)
           
        If (notes(i)) = note Then

            convertirNoteEnNombreDeLigne = i
            Exit For
        End If
        
    Next i
    
    

End Function

Public Function convertirTempoEnTemps(Tempo As Integer) As Double

    'Temps � attendre entre chaque note = 60 * 1/tempo
    Dim temps As Double
    temps = 60 / Tempo

    convertirTempoEnTemps = temps
    
End Function

'Utiliser cette m�thode pour charger une seul fois la liste
Private Sub RemplireNotes()
    
    'ligne -> note
    notes(31) = "DO3"
    notes(30) = "DOD3"
    notes(29) = "RE3"
    notes(28) = "RED3"
    notes(27) = "MI3"
    notes(26) = "FA3"
    notes(25) = "FAD3"
    notes(24) = "SOL3"
    notes(23) = "SOLD3"
    notes(22) = "LA3"
    notes(21) = "LAD3"
    notes(20) = "SI3"
    notes(19) = "DO4"
    notes(18) = "DOD4"
    notes(17) = "RE4"
    notes(16) = "RED4"
    notes(15) = "MI4"
    notes(14) = "FA4"
    notes(13) = "FAD4"
    notes(12) = "SOL4"
    notes(11) = "SOLD4"
    notes(10) = "LA4"
    notes(9) = "LAD4"
    notes(8) = "SI4"
    notes(7) = "DO5"
    
End Sub


'https://stackoverflow.com/questions/18602979/how-to-give-a-time-delay-of-less-than-one-second-in-excel-vba
Sub WaitFor(NumOfSeconds As Single)
    Dim SngSec As Single
    SngSec = Timer + NumOfSeconds

    Do While Timer < SngSec
        DoEvents
   Loop
End Sub

Function ResetCouleur(selec As Range, clearGreen As Boolean)

    Dim i As Integer

    For i = selec.Row To selec.Row + selec.Rows.count - 1 'On boulce sur les lignes de la selection
        For j = selec.Column To selec.Column + selec.Columns.count - 1 'On boulce sur les colonnes de la selection
            Dim vbColor As Long
            
            'La valeur de couleur de la celulle est grise si la couleur de la note du piano dessin� sur la m�me ligne est noir, sinon pas de couleur
            vbColor = IIf( _
                Cells(i, Utilitaire.ColonneMinimumPartition - 1).Interior.Color = vbBlack, _
                RGB(220, 220, 220), _
                xlNone _
            )
            
            'On change la couleur des celulles et on change la couleur de verte que si c'est sp�cifi� en argument de la fonction
            If Cells(i, j).Interior.Color <> vbGreen Or clearGreen Then
                Cells(i, j).Interior.Color = vbColor
            End If
            
        Next j
    Next i
End Function


Function construireRange(i1 As Integer, j1 As Integer, i2 As Integer, j2 As Integer) As Range
    Dim r As Range

    Set r = Range( _
        Cells(i1, j1), _
        Cells(i2, j2) _
    )

    Set construireRange = r
End Function

Sub cacherOutils()

    Application.ExecuteExcel4Macro "SHOW.TOOLBAR(""Ribbon"",False)"
    ActiveWindow.DisplayHeadings = False
    ActiveWindow.DisplayGridlines = False
    Application.DisplayFullScreen = True
    Application.DisplayStatusBar = Not Application.DisplayStatusBar
    Application.WindowState = xlMaximized
    ActiveWindow.WindowState = xlMaximized
    Application.DisplayFormulaBar = False
    Application.EnableEvents = True
    Application.ScreenUpdating = True

End Sub
