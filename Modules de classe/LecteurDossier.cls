VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "LecteurDossier"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'Chemin du dossier � scanner
Private cPath As String

Private Sub Class_Initialize()
    'On d�finie un chemin par d�faut
    cPath = ThisWorkbook.path + "\"
End Sub

Property Let path(path As String)
    cPath = path
End Property


Public Function listerFichiers() As String()

    'D�claration des variables
    Dim fichier As String
    Dim size As Integer
    Dim i As String
    Dim filesName() As String
    
    'la taille du tableau contenant le nom des fichiers est �gal au nombre de fichiers
    size = compterFichiers(Dir(cPath))
    ReDim filesName(0 To size)
    
    i = 0
    fichier = Dir(cPath) 'On r�cuper le nom du premier fichier dans le dossier
    
    Do While fichier <> ""
        filesName(i) = fichier 'On ajoute le fichier au tableau
        fichier = Dir 'On passe au fichier suivant en ne sp�cifiant aucun chemin
        i = i + 1
    Loop
    
    listerFichiers = filesName

End Function


Private Function compterFichiers(ByVal fichier As String) As Long
    
    Dim count As Long
    count = 0
    
    Do While fichier <> ""
        count = count + 1
        fichier = Dir
    Loop
    
    compterFichiers = count
    
End Function



