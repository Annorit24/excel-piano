VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Morceau"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private cTitre As String
Private cTempo As Integer
Private cArtiste As String
Private cNomFichier As String

Public Function init(Titre As String, Tempo As Integer, Artiste As String, NomFichier As String)

    cTitre = Titre
    cTempo = Tempo
    cArtiste = Artiste
    cNomFichier = NomFichier
    
End Function

Property Get Titre() As String
    Titre = cTitre
End Property

Property Get Tempo() As Integer
    Tempo = cTempo
End Property

Property Get Artiste() As String
    Artiste = cArtiste
End Property

Property Get NomFichier()
    NomFichier = cNomFichier
End Property
